﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="signup.aspx.cs" Inherits="signup" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="LoginStyleSheet.css" rel="stylesheet" type="text/css" />

    <title>Sign Up!</title>
</head>
<body>
    <form id="form2" runat="server">
        <div>
            <div class="login">
                <div class="login-screen">
                    <div class="app-title">
                        <h1>Create Account</h1>
                    </div>
                    <div class="login-form">

                        <asp:TextBox runat="server" ID="txtFirstName" CssClass="inputText" placeholder="First Name" />
                        <br />

                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorFirstName" runat="server"
                            ControlToValidate="txtFirstName"
                            ErrorMessage="Must enter first name"
                            ForeColor="Red"
                            Display="Dynamic">
                        </asp:RequiredFieldValidator>
                        <br />

                        <asp:TextBox runat="server" ID="txtLastName" CssClass="inputText" placeholder="Last Name" />
                        <br />

                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorLastName" runat="server"
                            ControlToValidate="txtLastName"
                            ErrorMessage="Must enter last name"
                            ForeColor="Red"
                            Display="Dynamic">
                        </asp:RequiredFieldValidator>
                        <br />

                        <asp:TextBox runat="server" ID="txtEmail" CssClass="inputText" TextMode="Email" placeholder="Email" />
                        <br />

                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmail" runat="server"
                            ControlToValidate="txtEmail"
                            ErrorMessage="Must enter email"
                            ForeColor="Red"
                            Display="Dynamic">
                        </asp:RequiredFieldValidator>
                        <br />

                        <asp:TextBox runat="server" ID="txtPassword" CssClass="inputText" TextMode="Password" placeholder="Password" />
                        <br />

                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorPassword" runat="server"
                            ControlToValidate="txtPassword"
                            ErrorMessage="Must enter a password"
                            ForeColor="Red"
                            Display="Dynamic">
                        </asp:RequiredFieldValidator>
                        <br />


                        <asp:TextBox runat="server" ID="txtConfirmPassword" CssClass="inputText" TextMode="Password" placeholder="Confirm Password" />
                        <br />

                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorConfirmPasword" runat="server"
                            ControlToValidate="txtConfirmPassword"
                            ErrorMessage="Must enter a confirm password"
                            ForeColor="Red"
                            Display="Dynamic">
                        </asp:RequiredFieldValidator>

                        <asp:CompareValidator ID="CompareValidatorConfirmPassword" runat="server"
                            ControlToValidate="txtConfirmPassword"
                            CssClass="ValidationError"
                            ControlToCompare="txtPassword"
                            ForeColor="Red"
                            Display="Dynamic"
                            ErrorMessage="Password do not match" />
                        <br />
                        <br />
                        <asp:Button runat="server" ID="btnSubmit" CssClass="btn" Text="Submit" OnClick="btnSumbit_OnClick" />
                        <br />
                        <asp:Label runat="server" ID="lblExistUser" Text="Have an account?" />
                        <asp:Label runat="server" ID="lblExistUser2" ForeColor="Red" Text="Email is already registered. Want to" Visible="false" />
                        <asp:LinkButton runat="server" ID="btnExistUser" Text="login" OnClick="btnExistUser_OnClick" CausesValidation="false"/>?
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>


