﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.Security.Cryptography;

public partial class signup : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSumbit_OnClick(object sender, EventArgs e)
    {

        try
        {
            User getValidation = new User();
            DataSet credentails = getValidation.checkEmail(txtEmail.Text.Trim());   // Verifies if the email exists in the database
            string email = credentails.Tables[0].Rows[0]["email"].ToString();   // Check if an email aleady exists
            lblExistUser.Visible = false;
            lblExistUser2.Visible = true;
        }
        catch (Exception)
        {
            if (txtPassword.Text.Trim() == txtConfirmPassword.Text.Trim())  // Checks if both the password an the confirm password are the same
            {
                string salt = saltFunction(15);
                string strHash = getPassword(txtPassword.Text.Trim(), salt);

                User user = new User();
                user.createUser(txtFirstName.Text, txtLastName.Text, txtEmail.Text, strHash, salt);
                Response.Redirect("~/Default.aspx");
            }
        }
    }

    protected void btnExistUser_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("~/Default.aspx");
    }

    public String saltFunction(int size)
    {
        var rng = new System.Security.Cryptography.RNGCryptoServiceProvider();
        var buff = new byte[size];
        rng.GetBytes(buff);
        return Convert.ToBase64String(buff);
    }

    public String GenerateSHA256Hash(String input, String salt)
    {
        byte[] tmpSource = System.Text.Encoding.UTF8.GetBytes(input + salt);
        byte[] tmpHash;
        tmpHash = new System.Security.Cryptography.SHA256Managed().ComputeHash(tmpSource);

        return ByteArrayToString(tmpHash);
    }

    String getPassword(string pass_str, string salt)
    {
        string sSourceData = pass_str;
        string strHash = GenerateSHA256Hash(txtPassword.Text, salt);
        return strHash;
    }

    static string ByteArrayToString(byte[] arrInput)
    {
        int i;
        StringBuilder sOutput = new StringBuilder(arrInput.Length);
        for (i = 0; i < arrInput.Length; i++)
        {
            sOutput.Append(arrInput[i].ToString("X2"));
        }
        return sOutput.ToString();
    }



}