﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href="LoginStyleSheet.css" rel="stylesheet" type="text/css" />

    <title>Triggered Ahoy! - Log In or Sign Up</title>
</head>

<body>
    <form id="form1" runat="server">
        <div>
        <div class="login">
            <div class="login-screen">
                <div class="app-title">
                    <h1>Triggered Ahoy!</h1>
                </div>
                <div class="login-form">
                    <div class="control-group">
                        <asp:TextBox runat="server" class="login-field" CssClass="inputText" ID="txtEmail" placeholder="Email" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                            ControlToValidate="txtEmail"
                            ErrorMessage="Please enter an email"
                            ForeColor="Red"
                            Display="Dynamic">
                        </asp:RequiredFieldValidator>
                    </div>
                    <br />
                    <div class="control-group">
                        <asp:TextBox runat="server" class="login-field" CssClass="inputText" ID="txtPassword" TextMode="Password" placeholder="Password" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                            ControlToValidate="txtPassword"
                            ErrorMessage="Please enter a password"
                            ForeColor="Red"
                            Display="Dynamic">
                        </asp:RequiredFieldValidator>
                        <asp:Label runat="server" ID="lblIncorrectCredentials" ForeColor="Red" Text="Incorrect email or password" Visible="false"></asp:Label>
                    </div>
                    <br />
                    <asp:Button runat="server" ID="btnSubmit" CssClass="btn" OnClick="btnSubmit_OnClick" Text="Login" />
                    <br />
                    Don't have an account? <asp:LinkButton runat="server" ID="btnNewUser" Text="sign up" OnClick="btnNewUser_OnClick" CausesValidation="false"/>
                </div>
            </div>
        </div>
            </div>
    </form>
</body>

</html>
