﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/User.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Pages_Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
    <section id="triggeredSubmit">
            <div class="newsFeed">
                <asp:TextBox runat="server" ID="textboxTriggered" CssClass="textbox" Placeholder="Trigger Someone" />
                <asp:LinkButton runat="server" ID="btnSubmit" OnClick="btnSubmit_OnClick" CssClass="submitButton" Text="Post" />
            </div>
            <asp:Repeater runat="server" ID="triggeredPost">
                <HeaderTemplate>
                    <table>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <table class="tablePanel">
                                <tr class="postedHeader">
                                    <td>Post By:
                                        <asp:Label ID="lblName" runat="server" Font-Bold="true" Text='<%#Eval("fullname") %>' />
                                        Posted:<asp:Label ID="lblDate" runat="server" Font-Bold="true" Text='<%#Eval("createDate") %>' /></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblTriggerPost" runat="server" CssClass="postedText" Text='<%#Eval("TriggeredEntered") %>' /></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lblNumberOfTriggers" runat="server" OnCommand="btnTriggered_OnClick" CommandName="Triggered" CommandArgument='<%# Eval("triggerid") %>'>You have triggered <%# Eval("triggers")%> people!</asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
    </section>
    <section id="topTenList">
        <div class="subPart">
            <h3>Top Ten Triggers</h3>
            <asp:Repeater runat="server" ID="triggeredPostTopTen">
                <HeaderTemplate>
                    <table>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblTriggerPost" runat="server" CssClass="postedTextTopTen" Text='<%#Eval("TriggeredEntered") %>' /></td>
                                    <td>
                                        <asp:LinkButton ID="lblNumberOfTriggers" runat="server" CommandArgument='<%# Eval("triggerid") %>'>triggers(<%# Eval("triggers")%>)</asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </section>
        </div>
    <div class="container">
    </div>

</asp:Content>

