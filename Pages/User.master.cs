﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Home : System.Web.UI.MasterPage
{
    private string user = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            user = Session["userid"].ToString();
        }
        catch (Exception)
        {
            Response.Redirect("~/Default.aspx");
        }
    }

    protected void lblNav_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("~/Pages/Home.aspx");
    }

    protected void btnTriggeredFeeds_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("~/Pages/Home.aspx");
    }

    protected void btnProfile_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("~/Pages/Profile.aspx");
    }

    protected void btnSettings_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("~/Pages/Settings.aspx");
    }

    protected void btnLogOut_OnClick(object sender, EventArgs e)
    {
        Session.Abandon();
        Response.Redirect("~/default.aspx");
    }
}
