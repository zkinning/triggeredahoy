﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Pages_Home : System.Web.UI.Page
{
    protected string user = "";
    protected string name = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            user = Session["userid"].ToString();
            User userName = new User();
            DataSet information = userName.getUserInformation(int.Parse(user));
            name = information.Tables[0].Rows[0]["fullname"].ToString();
        }
        catch (Exception)
        {
            Response.Redirect("~/Default.aspx");
        }

        /* Populate the most recent triggers */
        User data = new User();
        DataSet posts = data.getMostRecentTriggers();
        triggeredPost.DataSource = posts;
        triggeredPost.DataBind();

        DataSet topTenPosts = data.getTopTenPosts();
        triggeredPostTopTen.DataSource = topTenPosts;
        triggeredPostTopTen.DataBind();
    }

    protected void btnSubmit_OnClick(object sender, EventArgs e)
    {
        User submitTrigger = new User();
        submitTrigger.storeTriggeredPost(textboxTriggered.Text, int.Parse(user), name);
        textboxTriggered.Text = string.Empty;

        /* Populate the most recent triggers */
        User data = new User();
        DataSet posts = data.getMostRecentTriggers();
        triggeredPost.DataSource = posts;
        triggeredPost.DataBind();
    }

    protected void btnTriggered_OnClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Triggered")
        {
            int i = Convert.ToInt32(e.CommandArgument);
            User incrementTrigger = new User();
            incrementTrigger.incrementTrigger(i);
        }

    }
}