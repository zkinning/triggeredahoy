﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;


public partial class Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void btnNewUser_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("~/signup.aspx"); // Redirects the users to a sign up page
    }

    protected void btnSubmit_OnClick(object sender, EventArgs e)
    {
        try
        {
            User getValidation = new User();
            DataSet credentails = getValidation.checkEmail(txtEmail.Text.Trim());   // Verifies if the email exists in the database
            string email = credentails.Tables[0].Rows[0]["email"].ToString();   // Stores the email from the database into the string 'email'
            lblIncorrectCredentials.Visible = false;
            validateLogin(email);   // Calls validateLogin function
        }
        catch (Exception)
        {
            lblIncorrectCredentials.Visible = true;     // Displays an error message, informing the user that there was a "incorrect username or password"
            txtPassword.Text = string.Empty;
        }
    }

    // This function validates whether or not the credentials entered in by the user are valid
    protected void validateLogin(string email)
    {
        User getValidation = new User();
        DataSet credentails = getValidation.getValidation(txtEmail.Text.Trim());

        string salt = credentails.Tables[0].Rows[0]["salt"].ToString(); // Stores the salt that was assigned to the email entered in
        string strHash = getPassword(txtPassword.Text.Trim(), salt);    // Takes the password from the user input, appends the salt and then hashes it

        DataSet verifyValues = getValidation.verifyInputs(email, strHash);  // Calls the database to see in there is a matching user with the email and password entered in by the user

        try
        {
            Session.Add("userid", verifyValues.Tables[0].Rows[0]["userid"].ToString()); // Adds the user id to a session
            Response.Redirect("~/Pages/Home.aspx");   // Redirects the user to their home page
        }
        catch (Exception)
        {
            lblIncorrectCredentials.Visible = true;     // Displays an error message, informing the user that there was a "incorrect username or password"
            txtPassword.Text = string.Empty;
        }

    }

    public String GenerateSHA256Hash(String input, String salt)
    {
        byte[] tmpSource = System.Text.Encoding.UTF8.GetBytes(input + salt);
        byte[] tmpHash;
        tmpHash = new System.Security.Cryptography.SHA256Managed().ComputeHash(tmpSource);

        return ByteArrayToString(tmpHash);
    }

    String getPassword(string pass_str, string salt)
    {
        string sSourceData = pass_str;
        string strHash = GenerateSHA256Hash(txtPassword.Text, salt);
        return strHash;
    }

    static string ByteArrayToString(byte[] arrInput)
    {
        int i;
        StringBuilder sOutput = new StringBuilder(arrInput.Length);
        for (i = 0; i < arrInput.Length; i++)
        {
            sOutput.Append(arrInput[i].ToString("X2"));
        }
        return sOutput.ToString();
    }
}