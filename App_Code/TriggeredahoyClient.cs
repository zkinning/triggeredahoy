﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AceDataClient
/// </summary>
public class TriggeredahoyDataClient : TriggeredahoyData
{
    public int Tally = 0;
    public DataSet DataSetUpdate = new DataSet();
    public DataRow Dru;
    public DataRow dr;
    public DataSet ds;

    public SqlConnection Conn = new SqlConnection();
    public SqlCommand Cmd = new SqlCommand();
    public SqlDataAdapter Da = new SqlDataAdapter();
    public SqlDataAdapter DataAdapterUpdate = new SqlDataAdapter();
    public SqlCommandBuilder Scb = new SqlCommandBuilder();

    //loads the mentioned stored procedure
    public void LoadStoredProc(string csp)
    {
        try
        {
            Cmd = LoadCommandObject(csp);
            Conn = new SqlConnection(ConnectionString);
            Cmd.Connection = Conn;
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Parameters.Clear();
            Cmd.CommandTimeout = 1000;
            Cmd.CommandText = csp;
            Cmd.Connection.Open();
        }
        catch(Exception)
        {
        }
    }

 

   //functions or adding parameters of different types to stored procedure

    public void AddParameters(string sParamName, byte[] varParamValue)
    {
        Cmd.Parameters.AddWithValue(sParamName, varParamValue);
    }

    public void AddParameters(string sParamName, string varParamValue)
    {
        Cmd.Parameters.AddWithValue(sParamName, varParamValue);
    }

    public void AddParameters(string sParamName, int varParamValue)
    {
        Cmd.Parameters.AddWithValue(sParamName, varParamValue);
    }

    public void AddParameters(string sParamName, decimal varParamValue)
    {
        Cmd.Parameters.AddWithValue(sParamName, varParamValue);
    }

    public void AddParameters(string sParamName, long varParamValue)
    {
        Cmd.Parameters.AddWithValue(sParamName, varParamValue);
    }

    public void AddParameters(string sParamName, bool varParamValue)
    {
        Cmd.Parameters.AddWithValue(sParamName, varParamValue);
    }

    public void AddParameters(string sParamName, DataTable varParamValue)
    {
        Cmd.Parameters.AddWithValue(sParamName, varParamValue);
    }

    public void AddParameters(string sParamName, double varParamValue)
    {
        Cmd.Parameters.AddWithValue(sParamName, varParamValue);
    }

    public void AddParameters(string sParamName, DateTime varParamValue)
    {
        Cmd.Parameters.AddWithValue(sParamName, varParamValue);
    }

    public void AddParameters(string sParamName, Guid varParamValue)
    {
        Cmd.Parameters.AddWithValue(sParamName, varParamValue);
    }


    public DataSet FillStoredProc()
    {
        DataSet ds = new DataSet();
        Da = new SqlDataAdapter(Cmd);
        Da.Fill(ds, "ds");
        Cmd.Connection.Close();
        SetTally(ds);
        return ds;
    }

    public void ExecNonQuery()
    {
        Cmd.ExecuteNonQuery();
        Cmd.Connection.Close();
    }



    public string getSingleResult(string queryString)
    {

            SqlCommand command = new SqlCommand(queryString, this.Conn);
            command.Connection.Open();
            //command.ExecuteNonQuery();
            int rows = command.ExecuteNonQuery();
            try
            {
                return command.ExecuteScalar().ToString();
            }
            catch (Exception)
            {
                return "N/A";
            }
    }
    public  int ExecuteCommand(string queryString)
    {
        int rows = 0;
        Conn = new SqlConnection(ConnectionString);
 
            SqlCommand command = new SqlCommand(queryString, this.Conn);
            command.Connection.Open();
            rows = command.ExecuteNonQuery();
  
        return rows;
    }


    public void SetTally(DataSet dsTally)
    {
        Tally = 0;
        if (dsTally.Tables.Count > 0)
        {
            if (dsTally.Tables[0].Rows.Count > 0)
            {
                Tally = dsTally.Tables[0].Rows.Count;
            }
        }
    }

    public void close()
    {
        try
        {
            DataAdapterUpdate.Dispose();
        }
        catch { }

        try
        {
            Da.Dispose();
        }
        catch { }

        try
        {
            Cmd.Dispose();
        }
        catch { }

        try
        {
            Conn.Dispose();
        }
        catch { }

        try
        {
            DataSetUpdate.Dispose();
        }
        catch { }
        try
        {
            Scb.Dispose();
        }
        catch { }
    }

    public TriggeredahoyDataClient()
    {
    }
}