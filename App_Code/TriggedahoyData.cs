﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TriggeredahoyData
/// </summary>
public class TriggeredahoyData : Triggeredahoy.Triggeredahoy
{
    //Create Database Connection Objects
    public string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SQLConnection"].ToString();

    public DataSet dsUpdate = new DataSet();
    public SqlDataAdapter objUCmd;
    public SqlDataReader objReader = null;
    public string SecuredUsername = "";
    public string SecuredPassword = "";
    public SqlConnection objConn = null;
    public SqlDataAdapter objCmd = new SqlDataAdapter();
    public SqlCommandBuilder objUAuto;
    public SqlCommand objNCmd = null;



    public TriggeredahoyData()
    {
      
    }


    public TriggeredahoyData(string strConnection)
    {
        ConnectionString = strConnection;
        objConn = new SqlConnection(ConnectionString);
    }

    public void OpenConnection()
    {
        objConn = new SqlConnection(ConnectionString);
    }



    public SqlCommand LoadCommandObject(string spname)
    {
        OpenConnection();
        objNCmd = new SqlCommand();
        objNCmd.Connection = objConn;
        objNCmd.CommandType = CommandType.StoredProcedure;
        objNCmd.CommandText = spname;
        return objNCmd;

    }

}
