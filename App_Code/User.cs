﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for User
/// </summary>
public class User
{
    public User()
    {

    }

    public void storeTriggeredPost(string triggeredEntered, int userid, string name)
    {
        TriggeredahoyDataClient add = new TriggeredahoyDataClient();
        add.LoadStoredProc("triggeredahoy.dbo.storeTriggers");
        add.AddParameters("@triggeredEntered", triggeredEntered);
        add.AddParameters("@userid", userid);
        add.AddParameters("@name", name);
        add.ExecNonQuery();
    }

    public void createUser(string firstName, string lastName, string email, string password, string salt)
    {
        TriggeredahoyDataClient add = new TriggeredahoyDataClient();
        add.LoadStoredProc("triggeredahoy.dbo.createUser");
        add.AddParameters("@firstName", firstName);
        add.AddParameters("@lastName", lastName);
        add.AddParameters("@email", email);
        add.AddParameters("@password", password);
        add.AddParameters("@salt", salt);
        add.ExecNonQuery();
    }

    public void incrementTrigger(int triggerid)
    {
        TriggeredahoyDataClient add = new TriggeredahoyDataClient();
        add.LoadStoredProc("triggeredahoy.dbo.incrementTriggerPost");
        add.AddParameters("@triggerid", triggerid);
        add.ExecNonQuery();
    }


    public DataSet getValidation(string email)
    {
        TriggeredahoyDataClient add = new TriggeredahoyDataClient();
        add.LoadStoredProc("triggeredahoy.dbo.getValidation");
        add.AddParameters("email", email);
        return add.FillStoredProc();
    }

    public DataSet checkEmail(string email)
    {
        TriggeredahoyDataClient add = new TriggeredahoyDataClient();
        add.LoadStoredProc("triggeredahoy.dbo.checkEmail");
        add.AddParameters("@email", email);
        return add.FillStoredProc();
    }

    public DataSet verifyInputs(string email, string hashedPassword)
    {
        TriggeredahoyDataClient add = new TriggeredahoyDataClient();
        add.LoadStoredProc("triggeredahoy.dbo.validateLogin");
        add.AddParameters("@email", email);
        add.AddParameters("@hashedPassword", hashedPassword);
        return add.FillStoredProc();
    }

    public DataSet getUserInformation(int userid)
    {
        TriggeredahoyDataClient add = new TriggeredahoyDataClient();
        add.LoadStoredProc("triggeredahoy.dbo.getPatientInformation");
        add.AddParameters("@userid", userid);
        return add.FillStoredProc();
    }

    public DataSet getMostRecentTriggers()
    {
        TriggeredahoyDataClient add = new TriggeredahoyDataClient();
        add.LoadStoredProc("triggeredahoy.dbo.getMostRecentTriggers");
        return add.FillStoredProc();
    }

    public DataSet getTopTenPosts()
    {
        TriggeredahoyDataClient add = new TriggeredahoyDataClient();
        add.LoadStoredProc("triggeredahoy.dbo.getTopTenPosts");
        return add.FillStoredProc();
    }

}